//
//  PetManagerApp.swift
//  PetManager
//
//  Created by Morgan Stephens on 9/16/21.
//

import SwiftUI

@main
struct PetManagerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // initialize Amplify
        let _ = Backend.initialize()

        return true
    }
}
