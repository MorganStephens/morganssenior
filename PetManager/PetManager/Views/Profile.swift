//
//  Profile.swift
//
//  Profile.swift
//  PetManager
//
//  Created by Morgan Stephens on 10/20/21.
//

import SwiftUI


struct Profile: View {
    var body: some View {
        NavigationView {
                Form {
                    Section(header: Text("Information")) {
                        Text("Username" )
                        }
                    Section(header:Text("Pet")){
                        VStack{
                            Button(action: {
                            }){
                                NavigationLink("Pet Profiles",destination: PetProfile())
                            }
                        }
                        
                        
                    }
                    
                    }
            
        }
        .ignoresSafeArea()
        .navigationBarTitle("Profile")
    }
}


struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile()
    }
