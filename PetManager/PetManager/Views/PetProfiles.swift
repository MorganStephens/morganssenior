//
//  PetProfiles.swift
//  PetManager
//
//  Created by Morgan Stephens on 10/30/21.
//
import SwiftUI

struct Pet: Identifiable {
    var id = UUID()
    var name: String
    var type: String
    var age: String
}

struct PetRow: View {
    var pet: Pet
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(pet.name)
                Text(pet.type).font(.subheadline).foregroundColor(.gray)
            }
            Spacer()
            Text((pet.age + " years old") )
        }
    }
}

struct DetailView: View {
    var pet: Pet
    
    var body: some View {
        VStack {
            Text(pet.name).font(.title)
            
            Form {
                Section(header: Text("Information")){
                Text("\(pet.type)")
                Text(" \(pet.age + " years old")")
                }
                Section(header: Text("Vet Information")){
                Text("Doctor")
                Text("Address")
                }
                Section(header: Text("Vet Documents")){
                Text("Files")
                }
            }
            Text("Schedule")
            Spacer()
        }
    }
}

struct PetProfile: View {
    let petProfiles = [
        Pet(name: "Marly", type: "Dog", age:"10"),
        Pet(name: "Tucker", type: "Fish", age:"6"),
        Pet(name: "Lucky", type: "Cat", age:"3"),
    ]
    
    var body: some View {
        NavigationView {
            List(petProfiles) { pet in
                NavigationLink(destination: DetailView(pet: pet)){
                    PetRow(pet: pet)
                }
            }
            .navigationBarTitle("Pet Profiles")
        }
    }
}

struct PetProfiles_Previews: PreviewProvider {
    static var previews: some View {
        PetProfile()
    }

}
