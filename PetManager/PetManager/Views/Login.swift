//
//  Login.swift
//  PetManager
//
//  Created by Morgan Stephens on 9/21/21.
//

import SwiftUI

let storedusername = "bob"
let storedpassword = "admin123"

struct Login: View {
    
    @State private var username: String = ""
    @State private var password: String = ""
    @State var selection: Int? = nil
    
    @State var authenticationDidFail: Bool = false
    @State var authenticationDidSuccedd: Bool = false
    
    var body: some View {
        NavigationView{
        ZStack{
            LinearGradient(gradient: .init(colors:[Color("Color1"), Color("Color2"), Color("Color3")]), startPoint: .top, endPoint: .bottom).ignoresSafeArea()
            //Color("Color1").ignoresSafeArea()
            VStack{
                Text("Pet Manager")
                    .foregroundColor(Color(hue: 0.699, saturation: 1.0, brightness: 0.73))
                    .font(.largeTitle)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                Image("logo")
                            .resizable()
                            .scaledToFit()
                            .border(/*@START_MENU_TOKEN@*/Color.white/*@END_MENU_TOKEN@*/, width: 10)
                TextField("Enter Username", text: $username)
                    .foregroundColor(Color.black)
                    .padding()
                    .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                SecureField("Enter password", text: $password)
                    .foregroundColor(Color.black)
                    .padding()
                    .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                if authenticationDidFail {
                    Text("Information incorrect")
                }
                if authenticationDidSuccedd {
                    
                }
                NavigationLink(destination: Profile(), tag: 1, selection: $selection){
                Button(action: {
                    if self.username == storedusername && self.password == storedpassword{
                        self.authenticationDidSuccedd = true
                        self.selection = 1
                    } else {
                        self.authenticationDidFail = true
                    }
                }) {
                    Text("Login")
                        .padding()
                        .frame(minWidth: 0, maxWidth: 250)
                }
                }
                
                .background(Color.black)
                .foregroundColor(Color.white)
                .cornerRadius(25)
                Text("Don't Have an Account?")
                    .foregroundColor(.black.opacity(0.7))
                Button(action: {
                    
                }){
                    NavigationLink("Sign Up",destination: SignUp())
                }.foregroundColor(.blue)
            }
            .padding(.horizontal, 15)
            
        } }
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }

}

//struct Home : View {
  //  var body: some View{
        //NavigationView {
          //  NavigationLink(destination: )
        //}
    //}
//}
