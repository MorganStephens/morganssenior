//
//  Login.swift
//  PetManager
//
//  Created by Morgan Stephens on 10/21/21.
//

import SwiftUI

//let storedusername = "Admin"
//let storedpassword = "admin123"

struct SignUp: View {
    
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var email: String = ""
    
    @State var authenticationDidFail: Bool = false
    @State var authenticationDidSuccedd: Bool = false
    
    var body: some View {
        NavigationView{
        ZStack{
            LinearGradient(gradient: .init(colors:[Color("Color1"), Color("Color2"), Color("Color3")]), startPoint: .top, endPoint: .bottom).ignoresSafeArea()
            //Color("Color1").ignoresSafeArea()
            VStack{
                Text("Pet Manager")
                    .foregroundColor(Color(hue: 0.699, saturation: 1.0, brightness: 0.73))
                    .font(.largeTitle)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                Image("logo")
                            .resizable()
                            .scaledToFit()
                            .border(/*@START_MENU_TOKEN@*/Color.white/*@END_MENU_TOKEN@*/, width: 10)
                TextField("Enter Username", text: $username)
                    .foregroundColor(Color.black)
                    .padding()
                    .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                TextField("Enter Email", text: $email)
                    .foregroundColor(Color.black)
                    .padding()
                    .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                SecureField("Enter password", text: $password)
                    .foregroundColor(Color.black)
                    .padding()
                    .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                if authenticationDidFail {
                    Text("Information not correct")
                        .offset(y: -10)
                        .foregroundColor(.red)
                }
                if authenticationDidSuccedd {
                    Text("Information correct")
                }
                
                Button(action: {
                    if self.username == storedusername && self.password == storedpassword{
                        self.authenticationDidSuccedd = true
                    } else {
                        self.authenticationDidFail = true
                    }
                }) {
                    Text("Sign Up")
                        .padding()
                        .frame(minWidth: 0, maxWidth: 250)
                }
                .background(Color.black)
                .foregroundColor(Color.white)
                .cornerRadius(25)
                Text("Have an Account Already?")
                    .foregroundColor(.black.opacity(0.7))
                Button(action: {
                    
                }){
                    NavigationLink("Login",destination: Login())
                }.foregroundColor(.black)
            }
            .padding(.horizontal, 15)
            
        }}
    }
}

struct SignUp_Previews: PreviewProvider {
    static var previews: some View {
        SignUp()
    }

}

struct Home1 : View {
    var body: some View{
        VStack{
            Image("logo")
                .resizable()
                .frame(width:200, height:100)
            
            HStack {
                Button(action: {
                    
                }) {
                    Text("Exisiting")
                }
            }
        }
    }
}
